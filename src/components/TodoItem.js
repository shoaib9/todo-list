import React, { useEffect, useRef, useState } from "react";
import "../assets/scss/todo-item.scss";

const TodoItem = ({ todo, onCheck, onDelete, onEdit, index }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [text, setText] = useState(todo.value);
  const editInputRef = useRef(null);

  const onEditingText = (e) => {
    e.preventDefault();
    onEdit(index, text);
    setIsEditing(false);
  };

  useEffect(() => {
    if (editInputRef && editInputRef.current && isEditing) {
      editInputRef.current.focus();
    }
  }, [editInputRef, isEditing]);

  useEffect(() => {
    function handleClickOutside(event) {
      if (editInputRef.current && !editInputRef.current.contains(event.target)) {
        editInputRef.current.blur();
        setIsEditing(false);
      }
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [editInputRef]);

  return (
    <div className="todo-item">
      {isEditing ? (
        <form className="edit-todo-item" onSubmit={onEditingText}>
          <input
            className="edit-input"
            value={text}
            onChange={(e) => setText(e.target.value)}
            ref={editInputRef}
          />
        </form>
      ) : (
        <div className="todo-item-value">
          <input
            className="checkbox"
            type="checkbox"
            checked={todo.completed}
            onChange={(e) => onCheck(e.target.checked, index)}
          />
          <label
            className={todo.completed ? "striked" : ""}
            style={{
              backgroundImage: todo.completed
                ? "url(/images/filled-circle.svg)"
                : "url(/images/circle.svg)",
            }}
            onDoubleClick={() => setIsEditing(true)}
          >
            {todo.value}
          </label>
          <button onClick={() => onDelete(index)} className="delete-item">
            &#10005;
          </button>
        </div>
      )}
    </div>
  );
};

export default TodoItem;
