import React, { useEffect, useState } from "react";
import "../assets/scss/todo-list.scss";
import TodoItem from "./TodoItem";
import Footer from "./Footer";
import { FILTER } from "../assets/constants/GenContants";

const TodoList = () => {
  const [allTodos, setAllTodos] = useState([]);
  const [showTodos, setShowTodos] = useState([]);
  const [text, setText] = useState("");
  const [filter, setFilter] = useState(FILTER.ALL);
  const [activeTodosNumber, setActiveTodosNumber] = useState(0);
  const [areAllTodosChecked, setAreAllTodosChecked] = useState(false);

  useEffect(() => {
    const activeTodos = allTodos.filter((todo) => !todo.completed);
    setActiveTodosNumber(activeTodos.length);

    if (activeTodos.length > 0 || allTodos.length === 0)
      setAreAllTodosChecked(false);
    else setAreAllTodosChecked(true);

    if (filter === FILTER.COMPLETED) {
      const completedTodos = allTodos.filter((todo) => todo.completed);
      setShowTodos(completedTodos);
    } else if (filter === FILTER.ACTIVE) {
      setShowTodos(activeTodos);
    } else {
      setShowTodos([...allTodos]);
    }
  }, [filter, allTodos]);

  const onAddingTodo = (e) => {
    e.preventDefault();
    const todo = { value: text, completed: false };
    setAllTodos([...allTodos, todo]);
  };

  const onCheck = (checked, index) => {
    const changedTodo = { ...allTodos[index], completed: checked };
    const todos = [
      ...allTodos.slice(0, index),
      changedTodo,
      ...allTodos.slice(index + 1),
    ];
    setAllTodos([...todos]);
  };

  const onDelete = (index) => {
    const todos = [...allTodos.slice(0, index), ...allTodos.slice(index + 1)];
    setAllTodos([...todos]);
  };

  const onClearCompleted = () => {
    const todos = allTodos.filter((todo) => !todo.completed);
    setAllTodos([...todos]);
  };

  const onToggleAllTodos = () => {
    const checkedTodos = allTodos.map((todo) => {
      return { ...todo, completed: !areAllTodosChecked };
    });
    setAllTodos([...checkedTodos]);
    setAreAllTodosChecked(areAllTodosChecked);
  };

  const onEdit = (index, text) => {
    if (text.length === 0) {
      onDelete(index);
      return;
    }
    const changedTodo = { ...allTodos[index], value: text };
    const todos = [
      ...allTodos.slice(0, index),
      changedTodo,
      ...allTodos.slice(index + 1),
    ];
    setAllTodos([...todos]);
  };

  return (
    <>
      <h1 className="title">todos</h1>

      <div className="list-section">
        <form onSubmit={onAddingTodo} className="todo-input">
          <input
            value={text}
            placeholder="What needs to be done?"
            onChange={(e) => setText(e.target.value)}
          />
        </form>
        <span
          className={`toggle-all ${areAllTodosChecked ? "checked" : ""}`}
          onClick={onToggleAllTodos}
        />
        <section className="todo-list-items">
          {showTodos.map((todo, index) => (
            <TodoItem
              key={index}
              todo={todo}
              onCheck={onCheck}
              onDelete={onDelete}
              onEdit={onEdit}
              index={index}
            />
          ))}
        </section>
        <Footer
          activeTodosNumber={activeTodosNumber}
          allTodosNumber={allTodos.length}
          filter={filter}
          setFilter={setFilter}
          onClearCompleted={onClearCompleted}
        />
      </div>
    </>
  );
};

export default TodoList;
