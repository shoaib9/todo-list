import React from 'react';
import {FILTER} from "../assets/constants/GenContants";
import "../assets/scss/footer.scss"

const Footer = ({activeTodosNumber, allTodosNumber, filter, setFilter, onClearCompleted}) => {
    return (
        <footer className="footer">
            <span>{activeTodosNumber} items left</span>
            <div className="filter">
                {Object.values(FILTER).map((value) => (
                    <button
                        onClick={() => setFilter(value)}
                        className={filter === value && "selected"}
                    >
                        {value}
                    </button>
                ))}
            </div>

            <button
                onClick={onClearCompleted}
                className={`clear ${
                    allTodosNumber- activeTodosNumber === 0 && "not-active"
                }`}
            >
                Clear Completed
            </button>
        </footer>

    );
};

export default Footer;
